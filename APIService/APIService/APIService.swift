//
//  APIService.swift
//  APIService
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
import Models

public class APIService {
    
    private static let API_URL = "https://mocki.io/v1/6bb59bbc-e757-4e71-9267-2fee84658ff2"
    private static let IMAGE_URL_PARAMETER = "?raw=true"
    
    public init() { }
    
    /// Gets the product list
    ///
    /// - Parameters:
    ///   - completion: The closure to be executed once fetching the list.
    public func getProductList<T: Decodable>(completion: @escaping (Result<[T]>) -> Void) {
        
        self.fetch(url: APIService.API_URL, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    let productList = try decoder.decode([T].self, from: data)
                    completion(.success(productList))
                } catch let error {
                    completion(.failure(error))
                }
            }
        })
    }
    
    /// Downloads the product image
    ///
    /// - Parameters:
    ///   - id: Identity of the product
    ///   - completion: The closure to be executed once fetching image
    public func downloadImage(url: String, completion: @escaping (Result<Data>) -> Void) {
        fetch(url: url + APIService.IMAGE_URL_PARAMETER, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                completion(.success(data))
            }
        })
    }
    
    /// Fetch data
    ///
    /// - Parameters:
    ///   - url: URL of the data.
    ///   - completion: The closure to be executed once fetching data.
    private func fetch(url: String, completion: @escaping (Result<Data>) -> Void) {
        guard let url = URL(string: url) else { return }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { [weak self] data, response, error in
            guard self != nil else {
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(.failure(error ?? NSError()))
                }
                return
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
            
            DispatchQueue.main.async {
                completion(.success(data))
            }
        }
        
        task.resume()
    }
}
