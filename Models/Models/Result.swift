//
//  Result.swift
//  Models
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}
