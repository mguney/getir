//
//  ProductModel.swift
//  Models
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
import CoreData

public struct ProductModel: Decodable, Identifiable {
    
    enum CodingKeys : String, CodingKey {
        case productName
        case productDescription
        case productPrice
        case productImage
    }
    public var id = UUID()
    public let productName: String
    public let productDescription: String
    public let productPrice: Decimal
    public let productImage: String
    public var imageData: NSData?
}

extension ProductModel {
    public init(product: NSManagedObject) {
        self.init(id: product.value(forKey: "id") as! UUID,
                  productName: product.value(forKey: "productName") as! String,
                  productDescription: product.value(forKey: "productDescription") as! String,
                  productPrice: product.value(forKey: "productPrice") as! Decimal,
                  productImage: product.value(forKey: "productImage") as! String,
                  imageData: product.value(forKey: "imageData") as? NSData)
    }
}
