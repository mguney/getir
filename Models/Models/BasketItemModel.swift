//
//  BasketItemModel.swift
//  Models
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation

public struct BasketItemModel {
    public var id = UUID()
    public let productName: String
    public let productPrice: Decimal
    public var imageData: NSData?
    public var count: Int
    
    public init(id: UUID, productName: String, productPrice: Decimal, imageData: NSData?, count: Int) {
        self.id = id
        self.productName = productName
        self.productPrice = productPrice
        self.imageData = imageData
        self.count = count
    }
}
