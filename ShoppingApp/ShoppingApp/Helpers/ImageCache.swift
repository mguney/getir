//
//  ImageCache.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
import Models

public class ImageCache {
    
    public static let publicCache = ImageCache()
    
    private let cachedImages = NSCache<NSURL, NSData>()
    private var loadingResponses = [NSURL: [(ProductModel, NSData?) -> Swift.Void]]()
    
    public final func imageData(url: NSURL) -> NSData? {
        return cachedImages.object(forKey: url)
    }
    
    /// Returns the cached image if available, otherwise asynchronously loads and caches it.
    final func load(id: UUID, imageURL: String, completion: @escaping (NSData?) -> Swift.Void) {
        
        guard let url = NSURL(string: imageURL) else {
            completion(nil)
            return
        }
        
        if let cachedImage = imageData(url: url) {
            completion(cachedImage)
            return
        }
        
        loadFromService(id: id, url: url, completion: {data in
            completion(data)
        })
    }
    
    /// Returns the cached image if available, otherwise asynchronously loads and caches it.
    final func load(item: ProductModel, completion: @escaping (ProductModel, NSData?) -> Swift.Void) {
        
        guard let url = NSURL(string: item.productImage) else {
            completion(item, nil)
            return
        }
        
        if let cachedImage = imageData(url: url) {
            DispatchQueue.main.async {
                completion(item, cachedImage)
            }
            return
        }
        
        if loadingResponses[url] != nil {
            loadingResponses[url]?.append(completion)
            return
        } else {
            loadingResponses[url] = [completion]
        }
        
        loadFromService(id: item.id, url: url, completion: {data in
            guard let imageData = data else {
                completion(item, nil)
                return
            }
            guard let blocks = self.loadingResponses[url] else {
                completion(item, nil)
                return
            }
            
            for block in blocks {
                block(item, imageData as NSData)
                self.loadingResponses[url] = nil
                return
            }
        })
    }
    
    /// Go fetch the image.
    final func loadFromService(id: UUID, url: NSURL, completion: @escaping (NSData?) -> Swift.Void) {
        
        ServiceManager.instance.getImage(productId: id, url: url.absoluteString ?? "", completion: {result in
            switch result {
            case .failure(_):
                DispatchQueue.main.async {
                    completion(nil)
                }
            case .success(let data):
                DispatchQueue.main.async {
                    if let data = data {
                        // Cache the image.
                        self.cachedImages.setObject(data, forKey: url, cost: data.count)
                    }
                    completion(data)
                }
            }
        })
    }
}
