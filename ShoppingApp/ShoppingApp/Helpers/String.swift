//
//  String.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "\(self)_comment")
      }
}
