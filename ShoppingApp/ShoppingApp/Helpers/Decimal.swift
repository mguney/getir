//
//  Decimal.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation

extension Decimal {
    var formattedAmount: String {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = "$"
        formatter.numberStyle = .currency
        return formatter.string(from: self as NSDecimalNumber) ?? "0"
    }
}
