//
//  UIImage.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import UIKit

extension UIImage {
    func getThumbnail() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        
        let options = [
            kCGImageSourceCreateThumbnailWithTransform: true,
            kCGImageSourceCreateThumbnailFromImageAlways: true,
            kCGImageSourceThumbnailMaxPixelSize: 200] as CFDictionary
        
        guard let source = CGImageSourceCreateWithData(imageData as CFData, nil) else { return nil }
        guard let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options) else { return nil }
        
        return UIImage(cgImage: imageReference)
    }
}
