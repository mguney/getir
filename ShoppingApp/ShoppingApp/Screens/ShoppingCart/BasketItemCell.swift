//
//  BasketItemCell.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import UIKit

class BasketItemCell: UITableViewCell {

    @IBOutlet weak var prdouctCountLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
