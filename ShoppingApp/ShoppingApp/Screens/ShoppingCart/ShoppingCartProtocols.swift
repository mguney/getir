//
//  ShoppingCartProtocols.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

//MARK: Router
protocol ShoppingCartRouterProtocol: AnyObject {
    func showProductList()
}

//MARK: View
protocol ShoppingCartViewProtocol: AnyObject {
    func showProductList(productList: [BasketItemModel])
    func showSuccess(totalAmount: Decimal)
}

//MARK: Presenter
protocol ShoppingCartPresenterProtocol: AnyObject {
    func load()
    func checkout()
    func checkoutComplete()
}

//MARK: Interactor
protocol ShoppingCartInteractorProtocol: AnyObject {
    var delegate: ShoppingCartInteractorOutPutProtocol? { get set }
    func load()
    func checkout()
}

protocol ShoppingCartInteractorOutPutProtocol: AnyObject {
    func showProductList(productList: [BasketItemModel])
    func showSuccess(totalAmount: Decimal)
}

