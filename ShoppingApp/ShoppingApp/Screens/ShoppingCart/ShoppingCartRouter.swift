//
//  ShoppingCartRouter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ShoppingCartRouter: ShoppingCartRouterProtocol {
    
    unowned let view: ShoppingCartViewController
    weak var delegate: ProductListViewControllerDelegate?
    
    init(view: ShoppingCartViewController, delegate: ProductListViewControllerDelegate) {
        self.view = view
        self.delegate = delegate
    }
    
    static func build(delegate: ProductListViewControllerDelegate) -> ShoppingCartViewController {
        let view = ShoppingCartViewController()
        let router = ShoppingCartRouter(view: view, delegate: delegate)
        let interactor = ShoppingCartInteractor(service: ServiceManager.instance)
        let presenter = ShoppingCartPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
    
    func showProductList() {
        delegate?.refresh()
        view.navigationController?.popViewController(animated: true)
    }
}
