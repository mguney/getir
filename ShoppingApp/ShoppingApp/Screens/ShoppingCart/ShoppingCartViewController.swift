//
//  ShoppingCartViewController.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import UIKit
import Models

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    var productList: [BasketItemModel] = []
    var presenter: ShoppingCartPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ShoppingCart".localized
        tableView.register(UINib(nibName: "BasketItemCell", bundle: nil), forCellReuseIdentifier: "BasketItemCell")
        tableView.delegate = self
        tableView.dataSource = self
        presenter.load()
    }

    @IBAction func checkoutButtonTabbed(_ sender: Any) {
        presenter.checkout()
    }
}

extension ShoppingCartViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketItemCell", for: indexPath) as! BasketItemCell
        let product = productList[indexPath.row]
        if let imageData = product.imageData, let _ = imageData as Data?, let image = UIImage(data: imageData as Data) {
            cell.productImageView.image = image
        }
        cell.productTitleLabel.text = product.productName
        cell.productPriceLabel.text = product.productPrice.formattedAmount
        cell.prdouctCountLabel.text = String(product.count)
        return cell
    }
}

extension ShoppingCartViewController: ShoppingCartViewProtocol {
    func showSuccess(totalAmount: Decimal) {
        let alert = UIAlertController(title: "SucessTitle".localized, message: "Checkout Completed.\n Total Amount: \(totalAmount.formattedAmount)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: {_ in 
            self.presenter.checkoutComplete()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showProductList(productList: [BasketItemModel]) {
        self.productList = productList
        tableView.reloadData()
    }
}
