//
//  ShoppingCartPresenter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ShoppingCartPresenter: ShoppingCartPresenterProtocol {
    
    private unowned let view: ShoppingCartViewProtocol
    private let interactor: ShoppingCartInteractorProtocol
    private let router: ShoppingCartRouterProtocol
    
    init(view: ShoppingCartViewProtocol, interactor: ShoppingCartInteractorProtocol, router: ShoppingCartRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load()
    }
    
    func checkout() {
        interactor.checkout()
    }
    
    func checkoutComplete() {
        router.showProductList()
    }
}

extension ShoppingCartPresenter: ShoppingCartInteractorOutPutProtocol {
    
    func showProductList(productList: [BasketItemModel]) {
        view.showProductList(productList: productList)
    }
    
    func showSuccess(totalAmount: Decimal) {
        view.showSuccess(totalAmount: totalAmount)
    }
}
