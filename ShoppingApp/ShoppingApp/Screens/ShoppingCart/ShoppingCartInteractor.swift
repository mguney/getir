//
//  ShoppingCartInteractor.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import APIService
import Models

final class ShoppingCartInteractor: ShoppingCartInteractorProtocol {
    
    weak var delegate: ShoppingCartInteractorOutPutProtocol?
    private let service: ServiceProtocol
    private var productList: [ProductModel] = []
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func load() {
        let productList = service.getProductsInBasket()
        delegate?.showProductList(productList: productList)
    }
    
    func checkout() {
        let totalAmount = service.getBasketTotalAmount()
        service.removeAllData()
        delegate?.showSuccess(totalAmount: totalAmount)
    }
}

