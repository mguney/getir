//
//  ProductDetailProtocols.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

//MARK: Router
protocol ProductDetailRouterProtocol: AnyObject {
    func showProductList()
}

//MARK: View
protocol ProductDetailViewProtocol: AnyObject {
    func showDetail(productDetail: ProductModel, productCount: Int)
    func showImage(data: Data?)
}

//MARK: Presenter
protocol ProductDetailPresenterProtocol: AnyObject {
    func load()
    func update(productCount: Int)
}

//MARK: Interactor
protocol ProductDetailInteractorProtocol: AnyObject {
    var delegate: ProductDetailInteractorOutPutProtocol? { get set }
    func load()
    func update(productCount: Int)
}

protocol ProductDetailInteractorOutPutProtocol: AnyObject {
    func showProductDetail(product: ProductModel, productCount: Int)
    func showImage(data: Data?)
    func saveBasket()
}
