//
//  ProductDetailRouter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ProductDetailRouter: ProductDetailRouterProtocol {
    
    unowned let view: ProductDetailViewController
    
    init(view: ProductDetailViewController) {
        self.view = view
    }
    static func build(product: ProductModel) -> ProductDetailViewController {
        let view = ProductDetailViewController()
        let router = ProductDetailRouter(view: view)
        let interactor = ProductDetailInteractor(service: ServiceManager.instance, productDetail: product)
        let presenter = ProductDetailPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
    
    func showProductList() {
        view.navigationController?.popViewController(animated: true)
    }
}
