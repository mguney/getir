//
//  ProductDetailInteractor.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ProductDetailInteractor: ProductDetailInteractorProtocol {
    weak var delegate: ProductDetailInteractorOutPutProtocol?
    private let service: ServiceProtocol
    private var productDetail: ProductModel
    private var currentCountProductCount: Int = 0
    
    init(service: ServiceProtocol, productDetail: ProductModel) {
        self.service = service
        self.productDetail = productDetail
    }
    
    func load() {
        currentCountProductCount = self.service.getProductCountInBasket(productId: productDetail.id)
        self.delegate?.showProductDetail(product: productDetail, productCount: currentCountProductCount)
        
        if let imageData = productDetail.imageData {
            self.delegate?.showImage(data: imageData as Data)
        } else {
            ImageCache.publicCache.load(id: productDetail.id, imageURL: productDetail.productImage, completion: {[weak self] data in
                guard let self = self else { return }
                self.delegate?.showImage(data: data as Data?)
            })
        }
    }
    
    func update(productCount: Int) {
        service.updateProductCountInBasket(productId: productDetail.id, count: productCount, price: productDetail.productPrice)
        delegate?.saveBasket()
    }
}
