//
//  ProductDetailPresenter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ProductDetailPresenter: ProductDetailPresenterProtocol {
    
    private unowned let view: ProductDetailViewProtocol
    private let interactor: ProductDetailInteractorProtocol
    private let router: ProductDetailRouterProtocol
    
    init(view: ProductDetailViewProtocol, interactor: ProductDetailInteractorProtocol, router: ProductDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load()
    }
    
    func update(productCount: Int) {
        interactor.update(productCount: productCount)
    }
}

extension ProductDetailPresenter: ProductDetailInteractorOutPutProtocol {
    
    func showProductDetail(product: ProductModel, productCount: Int) {
        view.showDetail(productDetail: product, productCount: productCount)
    }
    
    func showImage(data: Data?) {
        view.showImage(data: data)
    }
    
    func saveBasket() {
        router.showProductList()
    }
    
}
