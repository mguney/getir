//
//  ProductDetailViewController.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import UIKit
import Models

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    var presenter: ProductDetailPresenter!
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    var productDetail: ProductModel?
    var productCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ProductDetail".localized
        tableView.register(UINib(nibName: "ProductDetailCell", bundle: nil), forCellReuseIdentifier: "ProductDetailCell")
        tableView.delegate = self
        tableView.dataSource = self
        presenter.load()
    }
    
    func updateProductCount(productCount: Int) {
        self.productCount = productCount
        productCountLabel.text = String(productCount)
    }
    
    @IBAction func stepperValueChanged(sender: UIStepper) {
        updateProductCount(productCount: Int(sender.value))
    }
    
    @IBAction func updateButtonTabbed(_ sender: Any) {
        presenter.update(productCount: productCount)
    }
}

extension ProductDetailViewController: ProductDetailViewProtocol {
    func showLoading() {
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
    }
    
    func showDetail(productDetail: ProductModel, productCount: Int) {
        self.productDetail = productDetail
        tableView.reloadData()
        stepper.value = Double(productCount)
        updateProductCount(productCount: productCount)
    }
    
    func showImage(data: Data?) {
        if let imageData = data, let _ = imageData as Data?, let _ = UIImage(data: imageData) {
            self.productDetail?.imageData = data as NSData?
            tableView.reloadData()
        }
    }
    
    func showError(error: Error) {
        let alert = UIAlertController(title: "ErrorTitle".localized, message: "GenericErrorMessage".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ProductDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailCell", for: indexPath) as! ProductDetailCell
        if let imageData = productDetail?.imageData, let _ = imageData as Data?, let image = UIImage(data: imageData as Data) {
            cell.productImageView?.image = image
        }
        
        cell.productTitleLabel.text = productDetail?.productName
        cell.productDescriptionLabel.text = productDetail?.productDescription
        cell.productPriceLabel.text = productDetail?.productPrice.formattedAmount
        return cell
    }
    
    
}



