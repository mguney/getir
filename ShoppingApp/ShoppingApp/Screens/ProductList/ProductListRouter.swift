//
//  ProductListRouter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ProductListRouter: ProductListRouterProtocol {
    
    unowned let view: ProductListViewController
    
    init(view: ProductListViewController) {
        self.view = view
    }
    
    static func build() -> ProductListViewController {
        let view = ProductListViewController()
        let router = ProductListRouter(view: view)
        let interactor = ProductListInteractor(service: ServiceManager.instance)
        let presenter = ProductListPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
    
    func showDetail(product: ProductModel) {
        let detailView = ProductDetailRouter.build(product: product)
        view.show(detailView, sender: nil)
    }
    
    func showShoppingCard() {
        let detailView = ShoppingCartRouter.build(delegate: view)
        view.show(detailView, sender: nil)
    }
}
