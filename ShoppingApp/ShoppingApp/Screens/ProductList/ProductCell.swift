//
//  ProductCell.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import UIKit
import Models

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func prepare(product: ProductModel) {
        if let imageData = product.imageData, let data = imageData as Data?, let image = UIImage(data: data) {
            productImageView.image = image
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
    }

}
