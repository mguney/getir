//
//  ProductListViewController.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import UIKit
import Models

class ProductListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var amountLabel: UILabel!
    
    var presenter: ProductListPresenter!
    var productList: [ProductModel] = []
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    var totalAmount: Decimal = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ProductList".localized
        
        addIndicator()
        prepareCollectionView()
        presenter.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        presenter.getTotalAmount()
    }
    
    func addIndicator() {
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
    }
    
    func prepareCollectionView() {
        collectionView.collectionViewLayout = createLayout()
        collectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        let product = productList[indexPath.row]
        cell.prepare(product: product)
        
        if product.imageData == nil {
            presenter.getImage(index: indexPath.row)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.selectProduct(product: productList[indexPath.row])
    }
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.5))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    @IBAction func basketButtonTabbed(_ sender: Any) {
        if totalAmount > 0 {
            presenter.showShoppingCard()
        } else {
            let alert = UIAlertController(title: "Warning".localized, message: "BasketEmptyMessage".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ProductListViewController: ProductListViewProtocol {
    func showLoading() {
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
    }
    
    func showProductList(productList: [ProductModel]) {
        self.productList = productList
        collectionView.reloadData()
    }
    
    func showImage(index: Int, data: Data?) {
        if let data = data as NSData?, data != productList[index].imageData {
            productList[index].imageData = data
            self.collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        }
    }
    
    func showError(error: Error) {
        let alert = UIAlertController(title: "ErrorTitle".localized, message: "GenericErrorMessage".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized.localized, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showTotalAmount(totalAmount: Decimal) {
        self.totalAmount = totalAmount
        amountLabel.text =  "Total Amount: \(totalAmount.formattedAmount)" 
    }
}

extension ProductListViewController: ProductListViewControllerDelegate {
    func refresh() {
        productList = []
        collectionView.reloadData()
        presenter.load()
    }
}

protocol ProductListViewControllerDelegate: AnyObject {
    func refresh()
}
