//
//  ProductListPresenter.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

final class ProductListPresenter: ProductListPresenterProtocol {
    
    private unowned let view: ProductListViewProtocol
    private let interactor: ProductListInteractorProtocol
    private let router: ProductListRouterProtocol
    
    init(view: ProductListViewProtocol, interactor: ProductListInteractorProtocol, router: ProductListRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load()
    }
    
    func selectProduct(product: ProductModel) {
        router.showDetail(product: product)
    }
    
    func getImage(index: Int) {
        interactor.getImage(index: index)
    }
    
    func getTotalAmount() {
        interactor.getTotalAmount()
    }
    
    func showShoppingCard() {
        router.showShoppingCard()
    }
}

extension ProductListPresenter: ProductListInteractorOutPutProtocol {
    func showTotalAmount(totalAmount: Decimal) {
        view.showTotalAmount(totalAmount: totalAmount)
    }
    
    func hideLoading() {
        view.hideLoading()
    }
    
    func showLoading() {
        view.showLoading()
    }
    
    func showProductList(productList: [ProductModel]) {
        view.showProductList(productList: productList)
    }
    
    func showImage(index: Int, data: Data?) {
        view.showImage(index: index, data: data)
    }
    
    func showError(error: Error) {
        view.showError(error: error)
    }
}
