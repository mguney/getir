//
//  ProductListProtocols.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import Models

//MARK: Router
protocol ProductListRouterProtocol: AnyObject {
    func showDetail(product: ProductModel)
    func showShoppingCard()
}

//MARK: View
protocol ProductListViewProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showProductList(productList: [ProductModel])
    func showImage(index: Int, data: Data?)
    func showError(error: Error)
    func showTotalAmount(totalAmount: Decimal)
}

//MARK: Presenter
protocol ProductListPresenterProtocol: AnyObject {
    func load()
    func selectProduct(product: ProductModel)
    func getImage(index: Int)
    func getTotalAmount()
    func showShoppingCard() 
}

//MARK: Interactor
protocol ProductListInteractorProtocol: AnyObject {
    var delegate: ProductListInteractorOutPutProtocol? { get set }
    func load()
    func getImage(index: Int)
    func getTotalAmount()
}

protocol ProductListInteractorOutPutProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showProductList(productList: [ProductModel])
    func showImage(index: Int, data: Data?)
    func showError(error: Error)
    func showTotalAmount(totalAmount: Decimal)
}

