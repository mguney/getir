//
//  ProductListInteractor.swift
//  ShoppingApp
//
//  Created by Mahmut on 21.05.2022.
//

import Foundation
import APIService
import Models

final class ProductListInteractor: ProductListInteractorProtocol {
    weak var delegate: ProductListInteractorOutPutProtocol?
    private let service: ServiceProtocol
    private var productList: [ProductModel] = []
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func load() {
        delegate?.showLoading()
        
        service.getProductList() { [weak self] (result: Result<[ProductModel]>) in
            guard let self = self else { return }
            self.delegate?.hideLoading()
            
            switch result {
            case .failure(let error):
                self.delegate?.showError(error: error)
                return
            case .success(let products):
                self.productList = products
                self.delegate?.showProductList(productList: products)
            }
        }
    }
    
    func getImage(index: Int) {
        let product = productList[index]
        print(index)
        ImageCache.publicCache.load(item: product, completion: {[weak self] fetchedItem, data in
            guard let self = self else { return }
            self.delegate?.showImage(index: index, data: data as Data?)
        })
    }
    
    func getTotalAmount() {
        delegate?.showTotalAmount(totalAmount: service.getBasketTotalAmount())
    }
}
