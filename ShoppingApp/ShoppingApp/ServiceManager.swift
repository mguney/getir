//
//  ServiceManager.swift
//  ShoppingApp
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
import APIService
import CoreDataService
import Models

public protocol ServiceProtocol {
    func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void)
    func getImage(productId: UUID, url: String, completion: @escaping (Result<NSData?>) -> Void)
    func getProductCountInBasket(productId: UUID) -> Int
    func updateProductCountInBasket(productId: UUID, count: Int, price: Decimal)
    func getBasketTotalAmount() -> Decimal
    func getProductsInBasket() -> [BasketItemModel]
    func removeAllData()
}

///This class is used to retrieve data.
public class ServiceManager: ServiceProtocol {
    
    static let PRODUCTS_NAME = "Products"
    static let TOTAL_AMOUNT_NAME = "TotalAmount"
    
    public static let instance: ServiceManager = ServiceManager()
    private let apiService: APIService
    private let coreDataService: CoreDataService
    
    init() {
        self.apiService = APIService()
        self.coreDataService = CoreDataService()
    }
    
    /// Gets the product list
    ///
    /// - Parameters:
    ///   - completion: The closure to be executed once fetching the list.
    public func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void) {
        
        if let productList = coreDataService.getProductList(), !productList.isEmpty {
            completion(.success(productList))
            return
        }
        
        apiService.getProductList() { [weak self] (result: Result<[ProductModel]>) in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let productList):
                self.coreDataService.saveProductList(productList: productList)
                completion(.success(productList))
            }
        }
    }
    
    /// Gets the product image
    ///
    /// - Parameters:
    ///   - productId: Identity of the product to be detailed.
    ///   - url: URL of the product image.
    ///   - completion: The closure to be executed once fetching image
    public func getImage(productId: UUID, url: String, completion: @escaping (Result<NSData?>) -> Void) {
        if let imageData = coreDataService.getProductImage(id: productId) {
            completion(.success(imageData))
            return
        }
        
        apiService.downloadImage(url: url, completion: {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                self.coreDataService.saveProductImage(id: productId, data: data as NSData)
                completion(.success(data as NSData))
            }
        })
    }
    
    /// Gets the products In Basket
    public func getProductsInBasket() -> [BasketItemModel] {
        let productCountList = getAllProductCounts()
        let items = getProductsInBasket(productIds: Array(productCountList.keys))
        var products: [BasketItemModel] = []
        
        for item in items {
            products.append(BasketItemModel(id: item.id, productName: item.productName, productPrice: item.productPrice, imageData: item.imageData, count: productCountList[item.id.uuidString] ?? 0))
        }
        
        return products
    }
    
    /// Get Product Count In Basket
    ///
    /// - Parameters:
    ///   - productId: Identity of the product to be detailed.
    public func getProductCountInBasket(productId: UUID) -> Int {
        let products = getAllProductCounts()
        return products[productId.uuidString] ?? 0
    }
    
    /// Update Product Count In Basket
    ///
    /// - Parameters:
    ///   - productId: Identity of the product to be updated.
    ///   - count: Product count.
    ///   - count: Product price.
    public func updateProductCountInBasket(productId: UUID, count: Int, price: Decimal) {
        let userDefaults = UserDefaults.standard
        var productCountList = getAllProductCounts()
        var totalAmount = getBasketTotalAmount()
        if count == 0 {
            if let productCount = productCountList[productId.uuidString]  {
                totalAmount = totalAmount - (Decimal(productCount) * price)
                productCountList.removeValue(forKey: productId.uuidString)
            }
        } else {
            if let productCount = productCountList[productId.uuidString]  {
                totalAmount = totalAmount - (Decimal(productCount) * price)
            }
            totalAmount = totalAmount + (Decimal(count) * price)
            productCountList[productId.uuidString] = count
        }
        userDefaults.set(String(describing: totalAmount), forKey: ServiceManager.TOTAL_AMOUNT_NAME)
        userDefaults.set(productCountList, forKey: ServiceManager.PRODUCTS_NAME)
    }
    
    /// Get Total Amount
    public func getBasketTotalAmount() -> Decimal {
        let userDefaults = UserDefaults.standard
        let totalAmount = userDefaults.object(forKey: ServiceManager.TOTAL_AMOUNT_NAME) as? String ?? "0"
        return Decimal(string: totalAmount) ?? 0
    }
    
    public func removeAllData() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: ServiceManager.TOTAL_AMOUNT_NAME)
        userDefaults.removeObject(forKey: ServiceManager.PRODUCTS_NAME)
        coreDataService.removeAllProducts()
    }
    
    /// Gets the product list from db
    ///
    /// - Parameters:
    ///   - productIds: Identities of the products.
    private func getProductsInBasket(productIds: [String]) -> [ProductModel] {
        return coreDataService.getProductList(productIds: productIds) ?? []
    }
    
    /// Gets all product list counts from basket
    private func getAllProductCounts() -> [String:Int] {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: ServiceManager.PRODUCTS_NAME) as? [String:Int] ?? [:]
    }
    
}
