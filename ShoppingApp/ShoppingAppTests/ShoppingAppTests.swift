//
//  ShoppingAppTests.swift
//  ShoppingAppTests
//
//  Created by Erdem Perşembe on 12.04.2022.
//

import XCTest
@testable import ShoppingApp
@testable import Models

class ShoppingAppTests: XCTestCase {
    
    private var service: MockServiceManager!
    private var productListPresenter: ProductListPresenter!
    private var productListInteractor: ProductListInteractor!
    private var productListView: MockProductListView!
    private var productListRouter: MockProductListRouter!
    
    override func setUp() {
        service = MockServiceManager()
        
        productListInteractor = ProductListInteractor(service: service)
        productListView = MockProductListView()
        productListRouter = MockProductListRouter()
        productListPresenter = ProductListPresenter(view: productListView, interactor: productListInteractor, router: productListRouter)
        productListView.presenter = productListPresenter
    }

    func testProductListLoad() throws {
        // Given:
        service.products = [ProductModel(id: UUID(), productName: "La Lorraine Tombul Ekmek",
                                         productDescription: "Doğal ham maddelerden üretilir.Koruyucu ve katkı maddesi içermez.İçindekiler: Un, su, maya, tuz",
                                         productPrice: 20, productImage: "https://github.com/android-getir/public-files/blob/main/images/5f36a28b29d3b131b9d95548_tr_1637858193743.jpeg",
                                         imageData: nil)]
        
        // When:
        productListView.viewDidLoad()
        
        // Then:
        XCTAssertEqual(productListView.isCallShowLoading, true)
        XCTAssertEqual(productListView.isCallHideLoading, true)
        XCTAssertEqual(productListView.productList.count, service.products.count)
    }
}

private final class MockProductListView: ProductListViewProtocol {
    
    
    var presenter: ProductListPresenter!
    
    func viewDidLoad() {
        presenter.load()
    }
    
    var isCallShowLoading: Bool = false
    var isCallHideLoading: Bool = false
    var totalAmount: Decimal = 0
    var productList: [ProductModel] = []
    
    func showLoading() {
        isCallShowLoading = true
    }
    
    func hideLoading() {
        isCallHideLoading = true
    }
    
    func showProductList(productList: [ProductModel]) {
        self.productList = productList
    }
    
    func showImage(index: Int, data: Data?) {
    }
    
    func showError(error: Error) {
        
    }
    
    func showTotalAmount(totalAmount: Decimal) {
        self.totalAmount = totalAmount
    }
}

private final class MockProductListRouter: ProductListRouterProtocol {
    func showDetail(product: ProductModel) {
        
    }
    
    func showShoppingCard() {
        
    }
}
