//
//  MockServiceManager.swift
//  ShoppingAppTests
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
@testable import Models
@testable import ShoppingApp

final class MockServiceManager: ServiceProtocol {
    
    var products: [ProductModel] = []
    
    func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void) {
        completion(.success(products))
    }
    
    func getProductCountInBasket(productId: UUID) -> Int {
        return 10
    }
    
    func updateProductCountInBasket(productId: UUID, count: Int, price: Decimal) {
        
    }
    
    func getBasketTotalAmount() -> Decimal {
        return 20
    }
    
    func getProductsInBasket() -> [BasketItemModel] {
        return []
    }
    
    func removeAllData() {
        
    }
    
    func getImage(productId: UUID, url: String, completion: @escaping (Result<NSData?>) -> Void) {
        
    }
    
    
    
}
