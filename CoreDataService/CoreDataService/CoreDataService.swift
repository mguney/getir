//
//  CoreDataService.swift
//  CoreDataService
//
//  Created by Mahmut on 22.05.2022.
//

import Foundation
import CoreData
import Models

///This class is used to connect to the CoreData and retrieve data.
public class CoreDataService {
    
    private static let CONTAINER_NAME = "ShoppingApp"
    private static let PRODUCT_ENTITY_NAME = "Product"
    private static let CREATED_DATE = "createdDate"
    private static let PRODUCT_NAME = "productName"
    private static let PRODUCT_DESCRIPTION = "productDescription"
    private static let PRODUCT_IMAGE = "productImage"
    private static let PRODUCT_PRICE = "productPrice"
    private static let IMAGE_DATA = "imageData"
    private static let ID = "id"
    
    public init() { }
    
    /// Gets the product list
    public func getProductList() -> [ProductModel]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataService.PRODUCT_ENTITY_NAME)
        
        let sort = NSSortDescriptor(key: CoreDataService.CREATED_DATE, ascending:true)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            let fetchResults = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject]
            return fetchResults?.map(ProductModel.init)
            
        } catch {
            return nil
        }
    }
    
    /// Gets the product list
    ///
    /// - Parameters:
    ///   - productIds: Identities of the products.
    public func getProductList(productIds: [String]) -> [ProductModel]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataService.PRODUCT_ENTITY_NAME)
        
        let sort = NSSortDescriptor(key: CoreDataService.CREATED_DATE, ascending:true)
        fetchRequest.sortDescriptors = [sort]
        let predicate = NSPredicate(format: "id IN %@", productIds)
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject]
            return fetchResults?.map(ProductModel.init)
            
        } catch {
            return nil
        }
    }
    
    /// Save the product list
    ///
    /// - Parameters:
    ///   - productList: List to be saved.
    public func saveProductList(productList: [ProductModel]) {
        let entityDescription = NSEntityDescription.entity(forEntityName: CoreDataService.PRODUCT_ENTITY_NAME, in: persistentContainer.viewContext)!
        
        for product in productList {
            let item = NSManagedObject(entity: entityDescription, insertInto: persistentContainer.viewContext)
            item.setValue(product.id, forKey: CoreDataService.ID)
            item.setValue(product.productName, forKey: CoreDataService.PRODUCT_NAME)
            item.setValue(product.productDescription, forKey: CoreDataService.PRODUCT_DESCRIPTION)
            item.setValue(product.productImage, forKey: CoreDataService.PRODUCT_IMAGE)
            item.setValue(product.productPrice, forKey: CoreDataService.PRODUCT_PRICE)
            item.setValue(Date(), forKey: CoreDataService.CREATED_DATE)
        }
        
        do {
            try persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    /// Gets the products image
    ///
    /// - Parameters:
    ///   - id: Identity of the product.
    public func getProductImage(id: UUID) -> NSData? {
        let predicate = NSPredicate(format: "id == %@", id as NSUUID)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: CoreDataService.PRODUCT_ENTITY_NAME)
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject, let imageData = product.value(forKey: CoreDataService.IMAGE_DATA) as? NSData else {
                return nil
            }
            return imageData
        } catch {
            return nil
        }
    }
    
    /// Save the products image
    ///
    /// - Parameters:
    ///   - id: Identity of the product.
    ///   - data: Data of the products image.
    public func saveProductImage(id: UUID, data: NSData) {
        let predicate = NSPredicate(format: "id == %@", id as NSUUID)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: CoreDataService.PRODUCT_ENTITY_NAME)
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject else {
                return
            }
            product.setValue(data, forKey: CoreDataService.IMAGE_DATA)
            try persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    /// Remove All Products
    public func removeAllProducts() {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataService.PRODUCT_ENTITY_NAME)
        
        do {
            if let fetchResults = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                for item in fetchResults {
                    persistentContainer.viewContext.delete(item)
                }
            }
        } catch {
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: CoreDataService.CONTAINER_NAME)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
