# Shopping App

I developed the application in a modular structure using the VIPER design pattern. You can see the flow of the screen in the protocol classes I prepared for the screens.

![alt text](https://kenanatmaca.com/wp-content/uploads/2019/01/16W73TuYu1DWi9JY4_Uh8aA.png)

There are 4 modules in the application
* Models -> Models used throughout the application are created in this module.
* CoreDataService -> Products and images of products are stored in CoreData. This module contains operations performed on CoreData.
* APIService -> In this module, products and images are taken over mock API.
* ShoppingApp -> Module where the main application is located

### Important informations:

* To install the app on your device you must run the ShoppingApp scheme.
* After the product list is retrieved from the mock api, it is saved in CoreData. Until the checkout is done, the data is displayed from CoreData.
* Product images are cached on CoreData and memory. NSCache is used to cache memory (ImageCache.swift).
* Products added to the cart are kept in UserDefaults.
* After checkout, all data including the product list is cleared.
* Unit test has been written for action that bring and list the product list.
